import requests
import lxml.html
from asset import Asset
# parser model

class Document(object):
    item_id = 0

    def __init__(self, url, body_selector_function, html=None):
        Document.item_id = Document.item_id + 1
        self.item_id = Document.item_id
        self.url = url
        if html is None:
            self.download()
        else:
            self.fake_download(html)
        self.body = body_selector_function(self.full_root)

    def download(self):
        request = requests.get(self.url)
        self.full_root = lxml.html.fromstring(request.content)
        self.full_root.make_links_absolute(self.url, handle_failures='ignore')
        
    def fake_download(self, html):
        self.full_root = lxml.html.fromstring(html)

    @property
    def assets(self):
        # TODO move to parser
        """ Get asset elements for each ASSET_TAG
        >>> d = Document('http://fakeurl.com/', lambda x: x, "<a href='cat'>dog<img src='pony'></a><a fail=true></a><img fail=true>")
        >>> list(x.original_url for x in d.assets)
        ['cat', 'pony']
        """
        ASSETS = ['.//a[@href]', './/img[@src]']
        assetid = 0
        # Assets should not overlap!
        for item in ASSETS:
            elements = self.body.xpath(item)
            self.asset_list = []
            for element in elements:
                self.asset_list.append(Asset(self, element, assetid))
                assetid = assetid + 1
        return self.asset_list

    def delete_asset(self, asset):
        # TODO
        pass

