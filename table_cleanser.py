from warnings import warn
import lxml.html

class TableCleanseError(Warning):
    pass

FORBIDDEN = ['p', 'div', 'br', 'hr', 'blockquote', 'dt', 'dl', 'dd', 'ol', 'ul', 'li', 'table', 'pre']

def cleanse(root):
    errored = False
    for tag in FORBIDDEN:
        path = './/table//{}'.format(tag)
        elements = root.xpath(path)
        if elements and not errored:
            warn("forbidden tag {}".format(tag), TableCleanseError)
            # errored = True
        for element in elements:
            if element.text is None:
                element.text = ' '
            else:
                element.text = ' '+unicode(element.text)+' '
            element.drop_tag()
    return root


if __name__ == '__main__':
    html = """
<html>
<table>
<tr><td>cat<br>dog</td></tr>
</table>
<table>
<tr><td><p>kitten</p></td></tr>
</table>
"""
    root = lxml.html.fromstring(html)
    print lxml.html.tostring(cleanse(root))
