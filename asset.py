import hashlib
import os
import requests
from collections import OrderedDict

import lxml.html  # TEST ONLY

URL_ATTRIBUTE = {'a': 'href', 'img': 'src'}
MANDATORY_OUTPUT = ['original_url', 'title', 'assetid',
                    'content_type', 'original_filename', 'filename']
DOWNLOAD_DIRECTORY = 'downloads'

def make_base_dir():
    try:
        os.mkdir(DOWNLOAD_DIRECTORY)
    except OSError:  # directory exists
        pass

def get_original_filename_from_disposition(content_disposition):
    """
    Given a 'content-disposition' header, return the filename.
    Used by download_asset

    >>> get_original_filename_from_disposition('attachment; filename=genome.jpeg; modification-date="Wed, 12 Feb 1997 16:29:51 -0500"')
    u'genome.jpeg'
    >>> get_original_filename_from_disposition('inline;filename="1-1973 PH-MOA.pdf"')
    u'1-1973 PH-MOA.pdf'
    """
    filename = None
    disp = content_disposition.split(';')
    for part in disp:
        if part.strip().startswith('filename='):
            assert filename is None
            filename = part.partition('=')[2]
            if filename[0] == '"' and filename[-1] == '"':
                filename = filename[1:-1]  # remove quotes
    u_filename = filename.decode('ascii', 'ignore')

    if u_filename != filename:
        logging.warn("unicode problem: {!r} != {!r}".format(u_filename, filename))  # TODO LOG PROPERLY
    assert u_filename is not None
    assert len(u_filename) > 0
    assert '/' not in u_filename
    assert '\\' not in u_filename
    return u_filename

class Asset(object):
    def __init__(self, parser, element, assetid):
        """parser:  the parent document.
                    parser.url is important, as is parser.root :)

           element: the tag of the parser which generated the asset
                    element.tag / element.attrib['href'/'alt'/'src']
                    are important

           assetid: the number in ASSET0"""
        self.parser = parser
        self.element = element
        assert isinstance(assetid, int)
        self._assetid = assetid
        self.response = None

    @property
    def source_url(self):
        return self.parser.url

    @property
    def original_url(self):
        return self.element.attrib.get(URL_ATTRIBUTE[self.element.tag])

    @property
    def title(self):
        return self.element.text_content()

    @property
    def assetid(self):
        return self._assetid

    def download(self):
        assert self.response is None
        self.response = requests.get(self.original_url)

    @property
    def content_type(self):
        assert self.response
        return self.response.headers.get('content-type').partition(';')[0]
 
    @property
    def original_filename(self):
        assert self.response
        disp = self.response.headers.get('content-disposition')
        if disp:
            return get_original_filename_from_disposition(disp)
        else:
            return self.original_url.split('/')[-1].replace('/','')

    @property
    def _directory(self):
        assert self.response
        subdir_name = hashlib.sha1(self.original_url).hexdigest()
        return os.path.join(DOWNLOAD_DIRECTORY, subdir_name)
     
    @property
    def filename(self):
        assert self.response
        new_filename = self.original_filename.partition('?')[0] or '_'
        return os.path.join(self._directory, new_filename)[:250]

    @property
    def output(self):
        assert self.response
        final_asset = OrderedDict()
        for item in MANDATORY_OUTPUT:
            final_asset[item] = getattr(self, item)
        return final_asset

    @property
    def is_valid(self):
        return verify.verify(self.url, exception=False, source_url=self.parser.url):
            
    # purge_if_not_valid

    def __repr__(self):
        final_asset = OrderedDict()
        for item in MANDATORY_OUTPUT:
            try:
                final_asset[item] = getattr(self, item)
            except AssertionError:
                pass
        return repr(final_asset)

    def save_file(self):
        assert self.response
        try:
            os.mkdir(self._directory)
        except OSError:
            pass

        with open(self.filename, 'w') as f:
            f.write(self.response.content)

    def save_db(self):
        assert self.response
        scraperwiki.sql.save(table_name='dl', data=self.output, unique_keys=['original_url'])

    def replace(self):
        def _blank_link=(_id='', link_format='#ASSET{}', text='ASSET_TAG')
            e = lxml.html.Element('a')
            e.attrib['href']=link_format.format(_id)
            e.text=text
            return e

        def _blank_img(_id='', link_format = '#ASSET{}', text='ASSET_TAG'):
            e = lxml.html.Element('img')
            e.attrib['src']=link_format.format(_id)
            return e

        def _blank_deleted(_id='', link_format, text):
            e = lxml.html.Element('deleteme')
            return e

        tag_functions = {'a': _blank_link, 'img': _blank_img}
        blank = tag_functions[self.element.tag](self.assetid)
        blank.tail = self.element.tail
        self.element.getparent().replace(self.element, blank)
        self.element = blank

    def delete(self):
        # hmmm...
        # X -> ASSET_TAG [onsite download/img]
        # X -> X [offsite link]
        # X -> ... [PDF icon image]
           
        self.element.tag = 'deleteme'
        self.replace()
        self.parser.delete(self)  #  TODO
