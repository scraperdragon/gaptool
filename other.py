def make_json(output_row):
    "Make json files in metadata directory, given a datastructure"
    output_directory = 'metadata'
    row_json = json.dumps(output_row, indent=2, ensure_ascii=False)
    filename = '{}.json'.format(output_row['_index_number'])
    file_path = os.path.join(output_directory, filename)
    with open(file_path, 'w') as f:
        f.write(row_json.encode('utf-8'))
