import html2text
html2text.BODY_WIDTH = 0
html2text.SKIP_INTERNAL_LINKS = False
html2text.UNICODE_SNOB = 1

def asciinate(html):
    return html2text.html2text(html)
